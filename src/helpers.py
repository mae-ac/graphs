from typing import List


def load_map(map_dir: str) -> (List[List[str]], List[List[str]]):
    with open(f"{map_dir}/terrain.txt", 'r') as handle:
        terrain = [list(row[:-1]) for row in handle.readlines()]
    with open(f"{map_dir}/biome.txt", 'r') as handle:
        biome = [list(row[:-1]) for row in handle.readlines()]
    return terrain, biome
