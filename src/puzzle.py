import pygame
import random
from typing import List

BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)

Board = List[List[int]]


class Canvas:
    def __init__(self, frame_rate: int = 30):
        self.frame_rate = frame_rate

    def setup(self, image_path: str, n: int, m: int, scaling_factor: float = 1.0):
        pygame.init()

        self.image = pygame.image.load(image_path)
        w, h = self.image.get_size()
        self.image = pygame.transform.scale(
            self.image, (w * scaling_factor, h * scaling_factor)
        )

        self.w = self.image.get_width() // m
        self.h = self.image.get_height() // n
        self.tiles = []
        self.board = []
        for i in range(n):
            row = []
            for j in range(m):
                rect = (j * self.w, i * self.h, self.w, self.h)
                tile = self.image.subsurface(pygame.Rect(rect))
                self.tiles.append(tile)
                row.append(i * m + j)
            self.board.append(row)
        self.tiles[-1] = None
        self.board[-1][-1] = -1

        self.screen = pygame.display.set_mode(self.image.get_size())
        self.clock = pygame.time.Clock()
        self.dt = 0

        self.running = True
        self.t = 0

    def handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT or (
                event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE
            ):
                self.running = False

    def blit(self):
        for i, row in enumerate(self.board):
            for j, t in enumerate(row):
                if t >= 0:
                    self.screen.blit(self.tiles[t], (j * self.w, i * self.h))

        for i, row in enumerate(self.board):
            for j, _ in enumerate(row):
                pygame.draw.rect(
                    self.screen,
                    BLACK,
                    (j * self.w, i * self.h, self.w, self.h),
                    width=1,
                )

    def shuffle(self, nb_rounds: int):
        n, m = (len(self.board), len(self.board[0]))
        i, j = (n - 1, m - 1)
        for _ in range(nb_rounds):
            self.handle_events()
            if not self.running:
                break

            possible_moves = []
            for di, dj in [(0, 1), (0, -1), (1, 0), (-1, 0)]:
                new_i, new_j = i + di, j + dj
                if 0 <= new_i < n and 0 <= new_j < m:
                    possible_moves.append((new_i, new_j))

            if possible_moves:
                new_i, new_j = random.choice(possible_moves)
                self.board[i][j], self.board[new_i][new_j] = self.board[new_i][new_j], self.board[i][j]
                i, j = new_i, new_j

            self.screen.fill(BLACK)
            self.blit()
            pygame.draw.rect(self.screen, RED, (0, 0, *self.image.get_size()), width=10)
            pygame.display.flip()
            self.clock.tick(self.frame_rate)

    def think(self):
        self.screen.fill(BLACK)
        self.blit()
        pygame.draw.rect(self.screen, BLUE, (0, 0, *self.image.get_size()), width=10)
        pygame.display.flip()
        self.clock.tick(self.frame_rate)

    def show_solution(self, solution: List[Board]):
        for b in solution:
            self.screen.fill(BLACK)
            self.board = b
            self.blit()
            pygame.draw.rect(self.screen, GREEN, (0, 0, *self.image.get_size()), width=10)
            pygame.display.flip()
            self.clock.tick(self.frame_rate)

    def loop(self):
        while self.running:
            self.handle_events()
            self.screen.fill(BLACK)
            self.blit()
            pygame.display.flip()
            self.dt = self.clock.tick(self.frame_rate) / 1000

            self.t += 1

        pygame.quit()
