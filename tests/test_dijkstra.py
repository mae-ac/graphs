GRAPH = {
    '1': {'2': 7, '3': 9, '6': 14},
    '2': {'1': 7, '3': 10, '4': 15},
    '3': {'1': 9, '2': 10, '4': 11, '6': 2},
    '4': {'2': 15, '3': 11, '5': 6},
    '5': {'4': 6, '6': 9},
    '6': {'1': 14, '3': 2, '5': 10},
}

EXPECTED = [
    ('1', '1', ['1']),
    ('1', '2', ['1', '2']),
    ('1', '3', ['1', '3']),
    ('1', '4', ['1', '3', '4']),
    ('1', '5', ['1', '3', '6', '5']),
    ('1', '6', ['1', '3', '6']),

    ('2', '1', ['2', '1']),
    ('2', '2', ['2']),
    ('2', '3', ['2', '3']),
    ('2', '4', ['2', '4']),
    ('2', '5', ['2', '4', '5']),
    ('2', '6', ['2', '3', '6']),

    ('3', '1', ['3', '1']),
    ('3', '2', ['3', '2']),
    ('3', '3', ['3']),
    ('3', '4', ['3', '4']),
    ('3', '5', ['3', '6', '5']),
    ('3', '6', ['3', '6']),

    ('4', '1', ['4', '3', '1']),
    ('4', '2', ['4', '2']),
    ('4', '3', ['4', '3']),
    ('4', '4', ['4']),
    ('4', '5', ['4', '5']),
    ('4', '6', ['4', '3', '6']),

    ('5', '1', ['5', '6', '3', '1']),
    ('5', '2', ['5', '4', '2']),
    ('5', '3', ['5', '6', '3']),
    ('5', '4', ['5', '4']),
    ('5', '5', ['5']),
    ('5', '6', ['5', '6']),

    ('6', '1', ['6', '3', '1']),
    ('6', '2', ['6', '3', '2']),
    ('6', '3', ['6', '3']),
    ('6', '4', ['6', '3', '4']),
    ('6', '5', ['6', '5']),
    ('6', '6', ['6']),
]


def test_dijkstra():
    from src.graph import dijkstra
    for s, e, expected in EXPECTED:
        path = dijkstra(GRAPH, s, e)
        assert path == expected, f"path from {s} to {e} should be {expected}, found {path}"


def test_dijkstra_with_priority_queue():
    from src.graph import dijkstra_with_priority_queue
    for s, e, expected in EXPECTED:
        path = dijkstra_with_priority_queue(GRAPH, s, e)
        assert path == expected, f"path from {s} to {e} should be {expected}, found {path}"


def test_dijkstra_on_the_fly():
    from src.graph import dijkstra_on_the_fly
    for s, e, expected in EXPECTED:
        path = dijkstra_on_the_fly(GRAPH, s, e)
        assert path == expected, f"path from {s} to {e} should be {expected}, found {path}"
