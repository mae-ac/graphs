def test_shuffling():
    import math
    from src.puzzle import shuffle_board

    def compute_permutation_signature(board):
        rows = len(board)
        cols = len(board[0])
        flatten = [board[i][j] for i in range(rows) for j in range(cols)]
        nb_permutations = 0
        for i in range(len(flatten)):
            for j in range(i + 1, len(flatten)):
                if flatten[i] > flatten[j]:
                    nb_permutations += 1
        return math.pow(-1, nb_permutations)

    board = shuffle_board(3, 3)
    valid = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, -1]
    ]
    assert compute_permutation_signature(board) == compute_permutation_signature(valid)

    invalid = [
        [1, 2, 3],
        [4, 5, 6],
        [8, 7, -1]
    ]
    assert compute_permutation_signature(valid) != compute_permutation_signature(invalid)

    board = shuffle_board(3, 4)
    valid = [
        [1, 2, 3, 4],
        [5, 6, 7, 8],
        [9, 10, 11, -1]
    ]
    invalid = [
        [1, 2, 3, 4],
        [5, 6, 7, 8],
        [9, 11, 10, -1]
    ]
    assert compute_permutation_signature(board) == compute_permutation_signature(valid)
    assert compute_permutation_signature(valid) != compute_permutation_signature(invalid)

    board = shuffle_board(4, 4)
    valid = [
        [1, 2, 3, 4],
        [5, 6, 7, 8],
        [9, 10, 11, 12],
        [13, 14, 15, -1]
    ]
    invalid = [
        [1, 2, 3, 4],
        [5, 6, 7, 8],
        [9, 10, 11, 12],
        [13, 15, 14, -1]
    ]
    assert compute_permutation_signature(board) == compute_permutation_signature(valid)
    assert compute_permutation_signature(valid) != compute_permutation_signature(invalid)


def test_neighbors():
    from src.puzzle import Puzzle, get_neighbors
    puzzle = Puzzle([
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, -1]
    ])

    neighbors = get_neighbors(puzzle)
    assert len(neighbors) == 2
    assert (Puzzle([
        [0, 1, 2],
        [3, 4, -1],
        [6, 7, 5]
    ]), 1) in neighbors
    assert (Puzzle([
        [0, 1, 2],
        [3, 4, 5],
        [6, -1, 7]
    ]), 1) in neighbors

    puzzle = Puzzle([
        [0, 1, 2],
        [3, -1, 4],
        [5, 6, 7]
    ])
    neighbors = get_neighbors(puzzle)
    assert len(neighbors) == 4
    assert (Puzzle([
        [0, -1, 2],
        [3, 1, 4],
        [5, 6, 7]
    ]), 1) in neighbors
    assert (Puzzle([
        [0, 1, 2],
        [3, 4, -1],
        [5, 6, 7]
    ]), 1) in neighbors
    assert (Puzzle([
        [0, 1, 2],
        [-1, 3, 4],
        [5, 6, 7]
    ]), 1) in neighbors
    assert (Puzzle([
        [0, 1, 2],
        [3, 6, 4],
        [5, -1, 7]
    ]), 1) in neighbors


def test_manhattan_distance():
    from src.puzzle import manhattan_distance, Puzzle
    solution = Puzzle([
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, -1]
    ])
    puzzles = [
        Puzzle([
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, -1]
        ]),
        Puzzle([
            [1, 2, 3],
            [4, 5, 6],
            [7, -1, 8]
        ]),
        Puzzle([
            [1, 2, 3],
            [4, 5, 6],
            [-1, 7, 8]
        ])
    ]

    distances = [0, 1, 2]

    for puzzle, distance in zip(puzzles, distances):
        assert manhattan_distance(puzzle, solution) == distance


def test_is_valid_path():
    from src.puzzle import Puzzle, is_valid_path
    path = [
        Puzzle([
            [0, 1, 2],
            [-1, 4, 5],
            [3, 6, 7]
        ]),
        Puzzle([
            [0, 1, 2],
            [3, 4, 5],
            [-1, 6, 7]
        ], "DOWN"),
        Puzzle([
            [0, 1, 2],
            [3, 4, 5],
            [6, -1, 7]
        ], "RIGHT"),
        Puzzle([
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, -1]
        ], "RIGHT")
    ]
    assert is_valid_path(path, path[0], path[-1])

    path = [
        Puzzle([
            [0, 1, 2],
            [-1, 4, 5],
            [3, 6, 7]
        ]),
        Puzzle([
            [0, 1, 2],
            [3, 4, 5],
            [-1, 6, 7]
        ], "DOWN"),
        Puzzle([
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, -1]
        ], "RIGHT"),
    ]
    assert not is_valid_path(path, path[0], path[-1])
